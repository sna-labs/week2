import subprocess
import shlex
import re

def run_shell_cmd(cmd):
    print(f'Executing command $ {cmd}')
    process = subprocess.run(cmd, stdout=subprocess.PIPE, shell=True)
    process.check_returncode()

    try:
        print(process.stdout.decode("utf-8"))
    except:
        print(process.stdout)

    return process.stdout


def parse_ssh_config(filename):
    res = {}
    with open(filename, 'r') as f:
        for l in f:
            if not l.startswith("#") and l.strip():
                key, value = re.split(r"\s+", l.strip(), 1)
                res[key] = value

    # set desired flags
    res["X11Forwarding"] = "no"
    res["UsePAM"] = "no"
    res["PasswordAuthentication"] = "no"

    return res

def create_ssh_config():
    with open('./sshd_config', 'w') as f:
        config = parse_ssh_config('./sshd_config')
        for key, value in config.items():
            f.write(f"{key} {value}\n")
    run_shell_cmd("docker exec sna2_ssh_server_1 /etc/init.d/ssh reload")

    # configure ssh public key non-password access
    run_shell_cmd("docker cp ./key.pub sna2_ssh_server_1:/root/.ssh/authorized_keys")
    run_shell_cmd("docker exec sna2_ssh_server_1 chmod 400 /root/.ssh/authorized_keys")
    run_shell_cmd("docker exec sna2_ssh_server_1 chown root:root /root/.ssh/authorized_keys")

    # give right permissions to private key
    run_shell_cmd("chmod 400 ./key")


def main():
    # check docker version
    run_shell_cmd("docker -v")

    # pull desired images
    run_shell_cmd("docker-compose pull")

    # check desired images
    run_shell_cmd("docker image ls")

    # shell script to get postgres data from zip archive
    # (as it cannot be kept in git repo as source)
    run_shell_cmd("if [[ ! -e postgres ]]; then unzip postgres.zip; fi")

    # run the containers
    run_shell_cmd("docker-compose -p sna2 up -d")

    # inspect the networks
    run_shell_cmd("docker inspect sna2_week2_external")
    run_shell_cmd("docker inspect sna2_week2_internal")

    # command for installing needed packages to container
    cmd = "apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -yq iputils-ping iproute2"
    run_shell_cmd(f"docker exec sna2_ssh_server_1 /bin/bash -c \"{cmd}\"")

    # as wiki server container has no access to internet, we need small hack to install packages there
    run_shell_cmd("docker network create week2_hack")
    run_shell_cmd("docker network connect week2_hack sna2_hackmd_wiki_1")
    run_shell_cmd(f"docker exec sna2_hackmd_wiki_1 /bin/bash -c \"{cmd}\"")
    run_shell_cmd("docker network disconnect week2_hack sna2_hackmd_wiki_1")
    run_shell_cmd("docker network rm week2_hack")

    # inspect the routing tables of containers
    run_shell_cmd("docker exec -it sna2_ssh_server_1 ip route list")
    run_shell_cmd("docker exec -it sna2_hackmd_wiki_1 ip route list")

    # show containers running
    run_shell_cmd("docker ps")

    # confirm wiki container is accessible from ssh server container
    run_shell_cmd("docker exec sna2_ssh_server_1 ping -c 1 hackmd_wiki")

    # confirm wiki container cannot access internet
    run_shell_cmd("docker exec sna2_hackmd_wiki_1 ping -c 1 8.8.8.8 || true")

    create_ssh_config()

    # create tunnel
    cmd = "ssh -L *:41489:hackmd_wiki:3000 -i ./key -p 41488 root@localhost"
    ssh_tunnel = subprocess.Popen(shlex.split(cmd))

    print('-' * 100)
    print(f"$ {cmd}")
    print("Go to <machine ip>:41489")
    ssh_tunnel.wait()

if __name__ == "__main__":
    main()







